<!DOCTYPE html>
<html>
    <?php include 'includes/head.php'; ?>
    <body>
        <?php include 'includes/header.php'; ?>
        <?php include 'sections/main_banner.php'; ?>
        <?php include 'sections/about.php'; ?>
        <?php include 'sections/services.php'; ?>
        <!-- <?php include 'sections/case_studies.php'; ?> -->
        <?php include 'sections/cta.php'; ?>
        <?php include 'includes/footer.php'; ?>
    </body>
</html>