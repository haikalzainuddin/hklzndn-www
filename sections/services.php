<section id="services" class="section-padding">
    <div class="container">
        <h2 class="section-title white">
            Services
        </h2>
        <div class="services-wrapper">
            <div class="service">
                <i class="fa fa-user"></i>
                <h4>Exhibitory Website</h3>
                <p>Whether it's for personal brand or for your company, I can build it for you. You don't only get a website, you'll be visible to the entire world as well!</p>
            </div>
            <div class="service">
                <i class="fa fa-user"></i>
                <h4>E-commerce Platform</h4>
                <p>Online shops are becoming a very demanding service nowadays. You don't want to be a step behind your competitors. Give your customers a new method of reaching your products</p>
            </div>
            <div class="service">
                <i class="fa fa-user"></i>
                <h4>UI/UX</h4>
                <p>Being in the industry for quite a while teaches me about design effectiveness. I'll be able to advise on the effectiveness of your current website and ways to improve it.</p>
            </div>
        </div>
    </div>
</section>