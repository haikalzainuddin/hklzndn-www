<section id="business-card" class="section-padding">
    <div class="biz-card-canvas front">
        <div class="biz-card-canvas-inner front">
            <div class="front-canvas">
                <div>
                    <div class="logo-name">hkl<span>zndn</span></div>
                    <div class="card-tagline">website<span> . e-commerce . </span>ui/ux</div>
                </div>
                <!-- <div class="card-bar"></div> -->
                <!-- <div class="card-tagline">website<span> . e-commerce . </span>ui/ux</div> -->
                <div class="card-details">
                    <h4>Haikal Zainuddin</h4>
                    <h5>Web Developer</h5>
                    <ul class="card-social">
                        <li>
                            +60 17 4383 016
                            <i class="fa fa-phone"></i> 
                        </li>
                        <li>
                            contact@hklzndn.com
                            <i class="fa fa-envelope"></i> 
                        </li>
                        <li>
                            https://www.hklzndn.com
                            <i class="fa fa-globe"></i> 
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="biz-card-canvas">
        <div class="biz-card-canvas-inner back">
            <div class="logo-name">hkl<span>zndn</span></div>
            <div class="card-bar"></div>
            <div class="card-tagline">website<span> . e-commerce . </span>ui/ux</div>
        </div>
    </div>

    <div class="biz-card-canvas front ">
        <div class="biz-card-canvas-inner front design-2">
            <div class="left">
                <div class="logo-name">hkl<span>zndn</span></div>
                <div class="card-tagline">website<span> . e-commerce . </span>ui/ux</div>
            </div>
            <div class="right">
                <div class="card-details">
                    <h4>Haikal Zainuddin</h4>
                    <h5>Web Developer</h5>
                    <ul class="card-social">
                        <li>
                            <i class="fa fa-phone"></i> 
                            +60 17 4383 016
                        </li>
                        <li>
                            <i class="fa fa-envelope"></i> 
                            contact@hklzndn.com
                        </li>
                        <li>
                            <i class="fa fa-globe"></i> 
                            https://www.hklzndn.com
                        </li>
                    </ul>
                </div>
            </div>
            <div class="front-canvas" style="display: none;">
                <div>
                    <div class="logo-name">hkl<span>zndn</span></div>
                    <div class="card-tagline">website<span> . e-commerce . </span>ui/ux</div>
                </div>
                <!-- <div class="card-bar"></div> -->
                <!-- <div class="card-tagline">website<span> . e-commerce . </span>ui/ux</div> -->
                <div class="card-details">
                    <h4>Haikal Zainuddin</h4>
                    <h5>Web Developer</h5>
                    <ul class="card-social">
                        <li>
                            +60 17 4383 016
                            <i class="fa fa-phone"></i> 
                        </li>
                        <li>
                            contact@hklzndn.com
                            <i class="fa fa-envelope"></i> 
                        </li>
                        <li>
                            https://www.hklzndn.com
                            <i class="fa fa-globe"></i> 
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="biz-card-canvas back ">
        <div class="biz-card-canvas-inner back design-2">
            <div class="logo-name">hkl<span>zndn</span></div>
            <div class="card-bar"></div>
            <div class="card-tagline">website<span> . e-commerce . </span>ui/ux</div>
        </div>
    </div>
</section>