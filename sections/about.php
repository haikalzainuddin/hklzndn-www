<section id="about" class="section-padding">
    <div class="container">
        <div class="wrapper">
            <div class="left">
                <img src="./assets/images/about-img.jpg"/>
            </div>
            <div class="right">
                <p>
                    Few years ago, I took part in web development and never looked back ever since. I enjoy looking at designs come to life, as well as looking at people being excited about their website being made available to the world.
                </p>
                <p>
                    I decided to make that a habit now. Pick up the phone and let's talk about how we can expand your digital reach and unlock your business' true potential.
                </p>
                <?php include './includes/social_media.php'; ?>
            </div>
        </div>
    </div>
</section>