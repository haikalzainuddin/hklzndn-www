<section id="cta" class="section-padding">
    <div class="container">
        <h2 class="section-title white center bigger">Lets talk about your project now!</h2>
        <a href="tel: +60174383016" class="main-btn">Go!</a>
    </div>
</section>