<section id="main-banner">
    <div class="container">
        <h1>Understanding your business, is my business.</h1>
        <div class="line white left wider"><span></span></div>
        <p>Building a website can be a hassle sometimes. Let me take care of that while you work on your brand as usual.</p>
    </div>
</section>