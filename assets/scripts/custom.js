$(window).ready(function(){
    var mobile_menu_btn = $('.mobile-menu-btn'),
        mobile_menu_box = $('.mobile-menu-box'),
        mobile_menu_close = $('.mobile-menu-close')

    mobile_menu_btn.click(function(e){
        e.stopPropagation();
        e.preventDefault();
        mobile_menu_box.addClass('show-this')
    })
    mobile_menu_close.click(function(e){
        e.stopPropagation();
        e.preventDefault();
        mobile_menu_box.removeClass('show-this')
    })
})