<a href="#" class="mobile-menu-btn"><i class="fa fa-bars"></i></a>

<div class="mobile-menu-box">
    <a href="#" class="mobile-menu-close"><i class="fa fa-times"></i></a>
    <ul class="mobile-menu-nav">
        <li><a href="tel: 60174383016"><i class="fa fa-phone"></i></a></li>
        <li><a href="mailto: contact@hklzndn.com"><i class="fa fa-envelope"></i></a></li>
        <!-- <li><a href="#">About</a></li>
        <li><a href="">Services</a></li>
        <li><a href="#">Case Studies</a></li> -->
    </ul>
</div>