<footer id="footer">
    <div class="container">
        <h4 class="logo-name alt">hkl<span>zndn</span></h4>
        <div class="line"><span></span></div>
        <a class="email" href="mailto: contact@hklzndn.com">contact@hklzndn.com</a>
        <?php include './includes/social_media.php'; ?>
    </div>
</footer>