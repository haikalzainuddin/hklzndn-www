<ul class="main-nav">
    <li><a href="#">About</a></li>
    <li><a href="#">Services</a></li>
    <li><a href="#">Case Studies</a></li>
    <li><a href="tel: 60174383016"><i class="fa fa-phone"></i></a></li>
    <li><a href="mailto: contact@hklzndn.com"><i class="fa fa-envelope"></i></a></li>
</ul>